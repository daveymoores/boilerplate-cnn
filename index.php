
<!doctype html>
<html lang="hu">
<head>

	<title>Boilerplate</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="UTF-8">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="./dist/styles/main.css" rel="stylesheet">

</head>
<body>

<div id="cnn-header">
	<a href="http://commercial.cnn.com/" class="cnn-back">Back to CNN International Commercial</a>
	<div class="cnn-menu">
		<a href="#">&copy; 2017 Cable News Network. Turner Broadcasting System Europe, limited. All Rights Reserved.</a>
		<a href="http://edition.cnn.com/terms">Terms of service</a>
		<a href="http://edition.cnn.com/privacy">Privacy guidelines</a>
		<a href="http://edition.cnn.com/#">Ad choices</a>
		<a href="http://edition.cnn.com/about">About us</a>
		<a href="http://edition.cnn.com/feedback">Contact us</a>
		<a href="http://www.turner.com/careers">Work for us</a>
		<a href="http://edition.cnn.com/help">Help</a>
	</div>
	<script type="text/javascript">(function(){if(!String.prototype.trim){String.prototype.trim=function(){return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,'');};} var add = function(n,c){ return (n.className = (rem(n,c) + ' ' + c).trim()); }, rem = function(n,c){ return (n.className = (' ' + n.className + ' ').replace(' ' + c + ' ', ' ').trim()); }, header = document.getElementById('cnn-header'), menu = header.querySelector('.cnn-menu'), isset = false, issetMenu = false; window.addEventListener('scroll', function(){ var toBeSet = (pageYOffset ? pageYOffset : (document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop)) > header.clientHeight / 2; if( !isset && toBeSet ){ add(header,'fixed'), isset = true; } else if( isset && !toBeSet ){ rem(header,'fixed'), isset = false; } }), menu.addEventListener('click', function( event ){ if( event.target === menu ){ if( !issetMenu ){ menu.className = add(menu,'active'), issetMenu = true; } else { menu.className = rem(menu,'active'), issetMenu = false; }} }); })();</script>
</div>

<section>

	<iframe id="iframe" src="https://docs.google.com/forms/d/e/1FAIpQLServ3QsNFgkVVeI0Eu6HgFDnodu7hc5iudHls6DTHaGs50wxA/viewform?embedded=true" width="100%" height="6500px" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>

</section>

<script type="text/javascript" src="./dist/scripts/main.min.js"></script>

</body>
</html>
